package M.T5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userName;
    TextView textW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userName = findViewById(R.id.username);
        textW = findViewById(R.id.textwatcher);
        findViewById(R.id.signin).setOnClickListener(this);
        findViewById(R.id.signup).setOnClickListener(this);



        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textW.setText(s.toString() + "@mail.com");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        textW.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                textW.setText(userName.getText().toString());
                return false;
            }
        });

    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signin) {
            String name = userName.getText().toString();
            if (name.equals("")) {
                Toast.makeText(this, "please Enter username", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "welcome " + name, Toast.LENGTH_SHORT).show();
                Log.d("signin", name);
            }

        } else if (v.getId() == R.id.signup) {
            Toast.makeText(this, "Waiting...", Toast.LENGTH_LONG).show();
            Log.d("signup", "register");

        }
    }
}
